import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';

import { WeatherRoutingModule } from './weather-routing.module';
import { WeatherComponent } from './weather.component';
import { WeatherService } from './weather.service';

@NgModule({
  imports: [CommonModule, TranslateModule, WeatherRoutingModule, FormsModule],
  declarations: [ WeatherComponent],
  providers: [WeatherService]
})
export class WeatherModule {}
