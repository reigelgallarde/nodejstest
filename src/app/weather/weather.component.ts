import { Component, OnInit } from '@angular/core';

import { WeatherService } from './weather.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {
  currentConditions: any[] = [];
  message: string;
  error: string;
  constructor(private weather: WeatherService) { }

  ngOnInit() {
  }

  getCurrentConditions(data: any) {
    this.weather.getCurrentConditions({zipcode: data.zipcode})
      .subscribe((response: any ) => {
        if (response.error) {
          this.error = response.error;
        }
        this.currentConditions = response.result;
        this.message = response.message;
      });
  }

}
