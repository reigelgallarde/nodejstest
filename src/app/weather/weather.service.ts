import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

export interface CurrentConditionOptions {
  zipcode: string;
}
@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  apiUrl = 'http://localhost:3000/weather';

  constructor(private http: HttpClient) { }

  getCurrentConditions( options: CurrentConditionOptions): Observable<any> {
    return this.http
      .get(this.apiUrl, {params: {zipcode: options.zipcode}})
      .pipe(
        catchError(() => of({ error: 'Error, could not load weather :-('}))
      );
  }
}
