# gallarde



# Serverless

1. Use terminal and go to server folder found in this project folder and install dependencies:
 ```
 npm install
 ```

2. Run server:
 ```
 serverless offline
 ```

# Angular


1. Use another terminal and go to project folder and install dependencies:
 ```
 npm install
 ```

2. Launch development server, and open `localhost:4200` in your browser:
 ```
 npm start
 ```

This project was generated with [ngX-Rocket](https://github.com/ngx-rocket/generator-ngx-rocket/)
version 6.2.1