'use strict';
var rp = require('request-promise');


module.exports.weather = async (event) => {
  var apiKey = 'wtOFlUlwgCAuSp0vaYfLA340GGmGkg3S';
  var apiLocationKeySearchPath = 'http://dataservice.accuweather.com/locations/v1/search';
  var apiLocationKeyJSON = 'http://dataservice.accuweather.com/currentconditions/v1/{{key}}.json'
  var zipCode = event.queryStringParameters.zipcode;
  var search = await rp({ uri: apiLocationKeySearchPath, qs: {apikey: apiKey, q: zipCode}, json: true});
  var message = 'No result found.'
  var result = '';
  var location;
  if (typeof search == 'object' && search.length) {
    console.log(search)
    if (search.length > 1) {
      message = 'Found ' + search.length + ' results. Selecting the first one: ' + search[0]['EnglishName'] + '.';
    }else{
      message = 'Found 1 result. ' + search['EnglishName'] + '.';
    }
    var locationKey = search[0]['Key'];
    result = await rp({
      uri: apiLocationKeyJSON.replace('{{key}}',locationKey),
      qs: {language: 'en', apikey: apiKey},
      json: true
    });
    location = search[0];
  }
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: message,
      result: result,
    }, null, 2),
  };
  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
